<?php

namespace kafka;


/**
 * @link https://arnaud.le-blanc.net/php-rdkafka-doc/phpdoc/class.rdkafka.html
 *
 * @method addBrokers(string $brokers)
 * @method getOutQLen()
 * @method newTopic(string $topicName, \RdKafka\TopicConf $topicConf = null)
 * @method poll(int $timeoutMs)
 * @method flush(int $timeoutMs)
 * @method purge(int $purgeFlags)
 */
class Producer
{

    /** @var \RdKafka\Producer */
    private $instance;


    public function __construct(\RdKafka\Conf $conf)
    {
        $this->instance = new \RdKafka\Producer($conf);
    }


    public function getKafkaProducerInstance() : \RdKafka\Producer
    {
        return $this->instance;
    }


    public function __call($name, $args)
    {
        return $this->instance->$name(...$args);
    }


    public function __destruct()
    {
        if ($this->instance instanceof \RdKafka\Producer) {
            $this->instance->flush(1000);
        }
    }

}